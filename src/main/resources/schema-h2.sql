create table stocks
(
    Symbol varchar(12),
    Value_Date date,
    Open float,
    High float,
    Low float,
    Close float,
    Adj_Close float,
    Volume int
);